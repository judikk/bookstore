
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AppComponent } from './app.component';
import { UserService } from './Shared/Services/user-service.service';
import { ParamInterceptor } from './Shared/Services/interceptor';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { ErrorService } from './Shared/Services/error.service';
import { BookStoreModule } from './Shared/BookStore/Components/bookstore.module';
import { AuthenticationModule } from './Shared/Authentication/Components/authentication.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';

 
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    BookStoreModule,
    AuthenticationModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    ToastContainerModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [
    UserService, {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true
    },
    ErrorService, {
      provide: ErrorHandler,
      useClass: ErrorService
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
