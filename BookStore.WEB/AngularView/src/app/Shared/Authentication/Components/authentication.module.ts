import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const appRoutes: Routes =[
  { path: 'register', component: RegistrationFormComponent },
  { path: 'login', component: LoginFormComponent},
];

@NgModule({
  declarations: [
    LoginFormComponent,
    RegistrationFormComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
})
export class AuthenticationModule { }
