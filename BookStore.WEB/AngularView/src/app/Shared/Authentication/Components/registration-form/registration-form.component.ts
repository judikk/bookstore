import { Component, OnInit } from '@angular/core';
import { RegistrationUserView } from '../../../Models/registration-user.view';
import { UserService } from '../../../Services/user-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  constructor(private httpService: UserService, private routerService: Router, private toast: ToastrService, private fb: FormBuilder) { }
  user: RegistrationUserView;
  form: FormGroup;
  userSex: boolean = false;

  onClickRegister() {

    if (this.userSex == true) {
      this.user.userSex = "male";
    }
    else {
      this.user.userSex = "female";
    }
    if (this.user.confirmPassword != this.user.password) {
      return this.toast.error("Confirm password is Incorrect", "Registration Feild")
    }
    if (this.form.invalid) {
      return this.toast.error("Fill all necessary fields.", "Registration Feild")
    }
    console.log(this.user);
    this.httpService.postSignIN(this.user).subscribe(
      res => {
      },
      error => console.log(error),
      () => {
        this.httpService.returnSeccesRegisterToast();
        this.routerService.navigate(['/login']);
      }
    );
  }
  ngOnInit() {

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required],
    });
    this.user = new RegistrationUserView();
  }

}
