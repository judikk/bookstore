import { Component, OnInit } from '@angular/core';
import { LoginUserView } from '../../../Models/login-user.view';
import { UserService } from '../../../Services/user-service.service';
import { Tokens } from 'src/app/Shared/Services/Halpers/Tokens';
import { ErrorService } from 'src/app/Shared/Services/error.service';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  TOKENS: Tokens;
  tokenData: any;
  constructor(private userService: UserService, private toast : ToastrService, private fb: FormBuilder) { }
  user: LoginUserView;
  form : FormGroup;
  onClickLogIn() {
    this.userService.postLogIN(this.user).subscribe(
      res => {
        console.log(res);
        this.TOKENS = res;
        var token = this.TOKENS.accessToken;
        this.tokenData = JSON.parse(atob(token.split('.')[1]));
      },
      err => console.log(err),
      () => {
        console.log(this.tokenData.confirmemail)
        if (this.tokenData.confirmemail == "False") {
          this.toast.error("Unauthorized"  ,'Email not confirmed : 401');
        }
        else {
          this.userService.returnSeccesLoginToast();
        }
        localStorage.setItem('accessToken', this.TOKENS.accessToken);
        localStorage.setItem('refreshToken', this.TOKENS.refreshToken);
      });
  }
  ngOnInit() {
    this.form = this.fb.group({
      name:  ['', Validators.required ],
      password:  ['', Validators.required ],
    });
    this.user = new LoginUserView();
  }

}
