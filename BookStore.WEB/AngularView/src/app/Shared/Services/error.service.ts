import { Injectable, ErrorHandler } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorService implements ErrorHandler {
  handleError(error: HttpErrorResponse) {
    if (error.status == 401 || error.status == 500)
    {
      this.toast.error(error.statusText, 'It\'s hapand again: ' + error.status);
    }

    if (error.status == 400)
    {
      this.toast.error(error.error, 'It\'s hapand again: ' + error.status);
    }
  }
  constructor(private toast: ToastrService) { }
}
