import { Injectable, ComponentFactoryResolver } from '@angular/core';
import { Observable, throwError, from, pipe } from 'rxjs';
import { retry, catchError, map, tap, delay, switchMap, } from 'rxjs/operators';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { UserService } from './user-service.service';
import { Tokens } from './Halpers/Tokens';
import { ToastrService } from 'ngx-toastr';


@Injectable({
    providedIn: 'root',
})

export class ParamInterceptor implements HttpInterceptor {

    constructor(private errorService: ErrorService, private userService: UserService, private toastsService: ToastrService, private http: HttpClient, ) {
    }
    TOKENS: Tokens = new Tokens();

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log(req);
        req = this.addHeaders(req);
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                this.errorService.handleError(error);
                if (error && error.status === 401) {
                    return this.postRefreshRequests(req, next);
                }
            }),
        )
    }

    postRefreshRequests(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.TOKENS.accessToken = this.userService.getAccessToken();
        this.TOKENS.refreshToken = this.userService.getRefreshToken();
        this.userService.postRefreshToken(this.TOKENS).subscribe(
            res => {
                this.TOKENS = res;
                console.log(res)
            },
            err => console.log(err),
            () => {
                localStorage.setItem('accessToken', this.TOKENS.accessToken);
                localStorage.setItem('refreshToken', this.TOKENS.refreshToken);
                this.toastsService.warning("Token is refreshed", "Relax");
                this.toastsService.success("Action compleate", "OK");
                this.http.post(req.url, req.body).subscribe(
                    res => {
                        console.log(res)
                    },
                    err => {
                        console.log(err)
                    }
                )
            });
        return next.handle(req);
    }

    addHeaders(req: HttpRequest<any>) {
        if (this.userService.getAccessToken() != null)
            req = req.clone({
                headers: req.headers.set('Content-Type', 'application/json').append('Authorization', `Bearer ${this.userService.getAccessToken()}`)
            });
        else
            req = req.clone({
                headers: req.headers.set('Content-Type', 'application/json')
            });
        return req;
    }
}

