import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AddBookView } from '../Models/add-book.view';
import { tap } from 'rxjs/operators';
import { AddAuthorView } from '../Models/add-authors.view';
import { AddGenreView } from '../Models/add-genre.view';
import { AddMagazineView } from '../Models/add-magazine.view';
import { DeleteBookView } from '../Models/romove-book.view';
import { DeleteAuthorView } from '../Models/remove-author.view';
import { DeleteMagazineView } from '../Models/remove-magazine.view';
import { DeleteGenreView } from '../Models/remove-genre.view';
import { UpdateAuthorView } from '../Models/update-author-view';
import { UpdateBookView } from '../Models/update-book.view';
import { UpdateGenreView } from '../Models/update-genre-view';
import { UpdateMagazineView } from '../Models/update-magazine.view';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient, private toast: ToastrService, private router: Router) { }

  getActions(): string[] {
    const array = ["Add", "Remove", "Update"];
    return array;
  }

  getItems(): string[] {
    const array = ["Book", "Author", "Magazine", "Genre", "User"];
    return array;
  }

  addBookView(model : AddBookView){
    const body = JSON.stringify(model);
    return this.http.post<AddBookView>('http://localhost:50015/api/Book/Add', body)
  }

  addAuthorView(model : AddAuthorView){
    const body = JSON.stringify(model);
    return this.http.post<AddAuthorView>('http://localhost:50015/api/Author/Add', body)
  }

  addGenreView(model : AddGenreView){
    const body = JSON.stringify(model);
    return this.http.post<AddGenreView>('http://localhost:50015/api/Genre/Add', body)
  }

  addMagazineView(model : AddMagazineView){
    const body = JSON.stringify(model);
    return this.http.post<AddMagazineView>('http://localhost:50015/api/Magazine/Add', body)
  }

  removeBookView(model : DeleteBookView){
    const body = JSON.stringify(model);
    return this.http.post<DeleteBookView>('http://localhost:50015/api/Book/Delete', body)
  }

  removeAuthorView(model : DeleteAuthorView){
    const body = JSON.stringify(model);
    return this.http.post<AddAuthorView>('http://localhost:50015/api/Author/Delete', body)
  }

  removeGenreView(model : DeleteGenreView){
    const body = JSON.stringify(model);
    return this.http.post<DeleteGenreView>('http://localhost:50015/api/Genre/Delete', body)
  }

  removeMagazineView(model : DeleteMagazineView){
    const body = JSON.stringify(model);
    return this.http.post<DeleteMagazineView>('http://localhost:50015/api/Magazine/Delete', body)
  }
  updateBookView(model : UpdateBookView){
    const body = JSON.stringify(model);
    return this.http.post<UpdateBookView>('http://localhost:50015/api/Book/Update', body)
  }

  updateAuthorView(model : UpdateAuthorView){
    const body = JSON.stringify(model);
    return this.http.post<UpdateAuthorView>('http://localhost:50015/api/Author/Update', body)
  }

  updateGenreView(model : UpdateGenreView){
    const body = JSON.stringify(model);
    return this.http.post<UpdateGenreView>('http://localhost:50015/api/Genre/Update', body)
  }

  updateMagazineView(model : UpdateMagazineView){
    const body = JSON.stringify(model);
    return this.http.post<UpdateMagazineView>('http://localhost:50015/api/Magazine/Update', body)
  }
}
