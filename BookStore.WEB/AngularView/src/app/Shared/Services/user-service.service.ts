import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Tokens } from './Halpers/Tokens';
import { RegistrationUserView } from '../Models/registration-user.view';
import { LoginUserView } from '../Models/login-user.view';



@Injectable({
  providedIn: 'root'
})
export class UserService{

  constructor(private http: HttpClient, private toast: ToastrService, private router : Router) {
  }

  postSignIN(body : RegistrationUserView) : Observable<RegistrationUserView>{
    const user = JSON.stringify(body);
    return this.http.post<RegistrationUserView>('http://localhost:50015/api/Account/Register', user);
  }

  postLogIN(body : LoginUserView) : Observable<Tokens>{
    const user = JSON.stringify(body);
    return this.http.post<Tokens>('http://localhost:50015/api/Account/LogIn', user);
  }

  postRefreshToken(body : Tokens): Observable<any>{
    const token = JSON.stringify(body);
    return this.http.post('http://localhost:50015/api/Account/CreateRefreshToken',  token);
  }

  public getRefreshToken() : string{
    return localStorage.getItem('refreshToken');
  }

  public getAccessToken() : string {
    return localStorage.getItem('accessToken');
  }
  
  returnSeccesLoginToast() : void{
    this.router.navigate(['']);
    this.toast.success('You are authorized', 'Registration is successful')
  }

  returnSeccesRegisterToast() : void{
    this.router.navigate(['/mainpage']);
    this.toast.success('Confirn your email', 'Welcome!')
  }
}
