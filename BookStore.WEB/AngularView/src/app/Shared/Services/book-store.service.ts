import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetAllBooksView } from '../Models/getalll-books.view';
import { GetAllAuthorsView } from '../Models/getall-authors.view';
import { GetAllGenresView } from '../Models/getall-genres.view';
import { GetAllMagazinesView } from '../Models/getall-magazines.view';
import { FilterBookView } from '../Models/filter-book.view';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class BookStoreService {
  constructor(private http: HttpClient) { }
  basketCount: string;
  getAllBooks(): Observable<GetAllBooksView> {
    return this.http.get<GetAllBooksView>("http://localhost:50015/api/Book/GetAll");
  }

  getAllAuthors(): Observable<GetAllAuthorsView> {
    return this.http.get<GetAllAuthorsView>("http://localhost:50015/api/Author/GetAll");
  }

  getAllGenre(): Observable<GetAllGenresView> {
    return this.http.get<GetAllGenresView>("http://localhost:50015/api/Genre/GetAll");
  }

  getAllMagazine(): Observable<GetAllMagazinesView> {
    return this.http.get<GetAllMagazinesView>("http://localhost:50015/api/Magazine/GetAll");
  }

  postFilterBooks(body: FilterBookView): Observable<FilterBookView> {
    return this.http.post<FilterBookView>("http://localhost:50015/api/Book/Filter", body);
  }

  tokenDecoder() {
    var token = localStorage.getItem("accessToken");
    const data = JSON.parse(atob(token.split('.')[1]));
    return data;
  }

  setBookBasket(value: string) {
    this.basketCount = localStorage.getItem("basketCount");
    localStorage.setItem(this.basketCount + " " + value, value);
  }
}
