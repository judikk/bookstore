import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookStoreMainComponent } from './book-store-main.component';

describe('BookStoreMainComponent', () => {
  let component: BookStoreMainComponent;
  let fixture: ComponentFixture<BookStoreMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookStoreMainComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
  