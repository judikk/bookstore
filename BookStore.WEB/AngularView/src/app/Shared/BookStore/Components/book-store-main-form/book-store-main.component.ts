import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { GetAllBooksView, BookGetAllBooksViewItem } from 'src/app/Shared/Models/getalll-books.view'
import { BookStoreService } from 'src/app/Shared/Services/book-store.service';
import { GetAllAuthorsView, BookGetAllAuthorsViewItem } from 'src/app/Shared/Models/getall-authors.view';
import { GetAllGenresView, GenreGetAllGenresViewItem } from 'src/app/Shared/Models/getall-genres.view';
import { GetAllMagazinesView, MagazineGetAllMagazinesViewItem } from 'src/app/Shared/Models/getall-magazines.view';
import { FilterBookView } from 'src/app/Shared/Models/filter-book.view';
import { CheckboxItem } from 'src/app/Shared/Services/Halpers/checkbox-item';
import { Options, PointerType } from 'ng5-slider';
import { AdminService } from 'src/app/Shared/Services/admin.service';
import { AddBookView } from 'src/app/Shared/Models/add-book.view';
import { ToastrService } from 'ngx-toastr';
import { AddAuthorView } from 'src/app/Shared/Models/add-authors.view';
import { AddGenreView } from 'src/app/Shared/Models/add-genre.view';
import { AddMagazineView } from 'src/app/Shared/Models/add-magazine.view';
import { DeleteBookView } from 'src/app/Shared/Models/romove-book.view';
import { DeleteGenreView } from 'src/app/Shared/Models/remove-genre.view';
import { DeleteMagazineView } from 'src/app/Shared/Models/remove-magazine.view';
import { DeleteAuthorView } from 'src/app/Shared/Models/remove-author.view';
import { UpdateBookView } from 'src/app/Shared/Models/update-book.view';
import { UpdateAuthorView } from 'src/app/Shared/Models/update-author-view';
import { UpdateGenreView } from 'src/app/Shared/Models/update-genre-view';
import { UpdateMagazineView } from 'src/app/Shared/Models/update-magazine.view';


@Component({
  selector: 'app-book-store-main',
  templateUrl: './book-store-main.component.html',
  styleUrls: ['./book-store-main.component.css']
})
export class BookStoreMainComponent implements OnInit {
  //Add
  public addBookView: AddBookView = new AddBookView();
  public addAuthorView: AddAuthorView = new AddAuthorView();
  public addGenreView: AddGenreView = new AddGenreView();
  public addMagazineView: AddMagazineView = new AddMagazineView();

  //Remove
  public removeBookView: DeleteBookView = new DeleteBookView();
  public removeAuthorView: DeleteAuthorView = new DeleteAuthorView();
  public removeMagazineView: DeleteMagazineView = new DeleteMagazineView();
  public removeGenreView: DeleteGenreView = new DeleteGenreView();

  public removeBookViewName;
  public removeAuthorViewName;
  public removeMagazineViewName;
  public removeGenreViewName;

  //Update
  public updateBookView: UpdateBookView = new UpdateBookView();
  public updateAuthorView: UpdateAuthorView = new UpdateAuthorView();
  public updateGenreView: UpdateGenreView = new UpdateGenreView();
  public updateMagazineView: UpdateMagazineView = new UpdateMagazineView();
  
  //Get
  public booksData: GetAllBooksView = new GetAllBooksView();
  public authorData: GetAllAuthorsView = new GetAllAuthorsView();
  public genreData: GetAllGenresView = new GetAllGenresView();
  public magazineData: GetAllMagazinesView = new GetAllMagazinesView();
  public filterBooksData: FilterBookView = new FilterBookView();

  authorCheckedFilter = new Array<CheckboxItem>();
  genreCheckedFilter = new Array<CheckboxItem>();
  magazineCheckedFilter = new Array<CheckboxItem>();
  booksInBasket = new Array<BookGetAllBooksViewItem>();
  //Helpers
  searchString: string;
  toPay: number = 0;
  basketCount: number = 0;
  tokenData: any;
  userRole: string = "";
  mapOfBasketValue = new Map<string, number>();
  //Admin
  action: string[] = [];
  adminItems: string[] = [];
  selectedAction: string;
  selectedAdminItems: string[] = [];
  selectedAdminAuthorData: BookGetAllAuthorsViewItem[] = [];
  selectedAdminGenreData: GenreGetAllGenresViewItem[] = [];
  selectedAdminMagazineData: MagazineGetAllMagazinesViewItem[] = [];
  //Constructor
  constructor(private bookService: BookStoreService, private adminService: AdminService, private toast : ToastrService) {
  }
  minValue: number = 0;
  maxValue: number = 10000;

  filterDate: Date[] = [];

  triggerFocus: EventEmitter<void> = new EventEmitter<void>();
  options: Options = {
    step: 1,
    floor: 10,
    ceil: 10000,
  };
  //Remove
  
  removeBook(){
    this.removeBookView.id = this.removeBookViewName.id;
    this.adminService.removeBookView(this.removeBookView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Book is removed", "Don't worry")
    );
  }
  
  removeAuthor(){
    this.removeAuthorView.id = this.removeAuthorViewName.id;
    this.adminService.removeAuthorView(this.removeAuthorView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Author is removed", "Don't worry")
    );
  }

  removeGenre(){
    this.removeGenreView.id = this.removeGenreViewName.id;
    this.adminService.removeGenreView(this.removeGenreView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Genre is removed", "Don't worry")
    );
  }

  removeMagazine(){
    this.removeMagazineView.id = this.removeMagazineViewName.id;
    this.adminService.removeMagazineView(this.removeMagazineView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Magazine is removed", "Don't worry")
    );
  }
  //Add
  addGenre(){
    this.adminService.addGenreView(this.addGenreView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Genre in database", "Don't worry")
    )
  }

  addAuthor(){
    this.adminService.addAuthorView(this.addAuthorView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Author in database", "Don't worry")
    )
  }
  addMagazine(){
    this.adminService.addMagazineView(this.addMagazineView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Magazine in database", "Don't worry")
    )
  }

  addBook() {
    this.selectedAdminAuthorData.forEach(adminAuthors => {
      this.addBookView.authorsID.push(adminAuthors.id);
    });
    this.selectedAdminGenreData.forEach(adminGenres => {
      this.addBookView.genresID.push(adminGenres.id);
    })
    this.selectedAdminMagazineData.forEach(adminMagazines => {
      this.addBookView.magazinesID.push(adminMagazines.id);
    })
    console.log(this.addBookView);
    this.adminService.addBookView(this.addBookView).subscribe(
      res => console.log(res),
      err => console.log(err),
      () => this.toast.success("Book in database", "Don't worry")
    )
    this.addBookView = new AddBookView();
  }

  //Get
  totalPrice(price: number, count: number) {
    return price * count;
  }

  removeItemFromBasket(value: string) {
    var i = localStorage.getItem(value);
    if (Number(i) != 0) {
      localStorage.removeItem(i + " " + value);
      localStorage.setItem(value, (Number(i) - 1).toString());
      this.mapOfBasketValue.set(value, (Number(i) - 1));
      this.basketCount = this.basketCount - 1;
      this.booksData.books.forEach(element => {
        if (element.id == value) {
          this.toPay = this.toPay - element.bookPrice;
          const index: number = this.booksInBasket.indexOf(element);
          if (index !== -1) {
            this.booksInBasket.splice(index, 1);
          }
        }
      });
    }
  }

  cleanLocalStorage() {
    this.booksData.books.forEach(value => {
      this.basketCount = Number(localStorage.getItem("basketCount"));
      for (let i = 0; i <= this.basketCount; i++) {
        localStorage.removeItem(i + " " + value.id)
        localStorage.removeItem(value.id)
      }
    });
    this.basketCount = 0;
    this.toPay = 0;
    this.mapOfBasketValue = new Map();
    localStorage.removeItem("basketCount");
    localStorage.removeItem("TOPAY");
    this.booksInBasket = new Array<BookGetAllBooksViewItem>();
  }

  onAddClick(value: string) {
    if (this.mapOfBasketValue.get(value) == null) {
      this.mapOfBasketValue.set(value, 1);
      this.basketCount = this.basketCount + 1;
    }
    else {
      var count = this.mapOfBasketValue.get(value);
      this.mapOfBasketValue.delete(value);
      this.mapOfBasketValue.set(value, count + 1);
      this.basketCount = this.basketCount + 1;
    }
    localStorage.setItem(value, this.mapOfBasketValue.get(value).toString());
    console.log(this.mapOfBasketValue);
    var count = Number(localStorage.getItem("basketCount"));
    if (count == null) count = 0;
    localStorage.setItem("basketCount", (count + 1).toString());
    this.bookService.setBookBasket(value);
    this.booksData.books.forEach(element => {
      if (element.id == value) this.booksInBasket.push(element);
    });
    this.toPay = 0;
    this.booksInBasket.forEach(x => {
      this.toPay += x.bookPrice;
    });
    localStorage.setItem("TOPAY", this.toPay.toString());
  }


  onChange(): void {
    this.filterBooksData.searchString = this.searchString;
    this.filterBooksData.EndPrice = this.maxValue;
    this.filterBooksData.StartPrice = this.minValue;
    this.filterBooksData.maxDate = this.filterDate[1];
    this.filterBooksData.minDate = this.filterDate[0];

    console.log(this.filterDate);

    this.authorCheckedFilter.forEach(checkedValue => {
      if (checkedValue.checked) {
        this.filterBooksData.authorID.push(checkedValue.value);
      }
    });
    this.genreCheckedFilter.forEach(checkedValue => {
      if (checkedValue.checked) {
        this.filterBooksData.genreID.push(checkedValue.value);
      }
    });
    this.magazineCheckedFilter.forEach(checkedValue => {
      if (checkedValue.checked) {
        this.filterBooksData.magazineID.push(checkedValue.value);
      }
    });
    this.bookService.postFilterBooks(this.filterBooksData).subscribe(res => {
      this.filterBooksData = res;
      this.booksData.books.forEach(value => {
        value.enabled = false;
      });
      console.log(res);
      this.filterBooksData.filterBooks.forEach(element =>
        this.booksData.books.forEach(value => {
          if (value.id == element.id)
            value.enabled = true;
        }));
    },
      err => {
        console.log(err);
      })

    this.filterDate = [];
  }



  ngOnInit() {

    this.bookService.getAllBooks().subscribe(res => {
      this.booksData = res;
      this.booksData.books.forEach(check => check.enabled = true);
      this.booksData.books.forEach(value => {
        if (localStorage.getItem(value.id) != null) {
          this.basketCount = this.basketCount + Number(localStorage.getItem(value.id));
          this.mapOfBasketValue.set(value.id, Number(localStorage.getItem(value.id)));
        }

      });
      console.log(res);
    },
      err => console.log(err),
    );

    this.bookService.getAllAuthors().subscribe(res => {
      this.authorData = res;
      this.authorData.authors.forEach(val => {
        const item = new CheckboxItem(val.id, false);
        this.authorCheckedFilter.push(item);
      });
      console.log(res);
    },
      err => console.log(err),
    );

    this.bookService.getAllGenre().subscribe(res => {
      this.genreData = res;
      this.genreData.genres.forEach(val => {
        const item = new CheckboxItem(val.id, false);
        this.genreCheckedFilter.push(item);
      });
      console.log(res);
    },
      err => console.log(err),
    );

    this.bookService.getAllMagazine().subscribe(res => {
      this.magazineData = res;
      this.magazineData.magazines.forEach(val => {
        const item = new CheckboxItem(val.id, false);
        this.magazineCheckedFilter.push(item);
      });
      console.log(res);
    },
      err => console.log(err),
    );
    this.toPay = Number(localStorage.getItem("TOPAY"));
    if (localStorage.accessToken != null) {
      this.tokenData = localStorage.getItem("accessToken");
      this.tokenData = this.bookService.tokenDecoder();
      this.userRole = this.tokenData['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
      console.log(this.tokenData);
    }
    if (this.userRole == 'admin') {
      this.action = this.adminService.getActions();
      this.adminItems = this.adminService.getItems();
    }
  }
}
