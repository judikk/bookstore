import { NgModule } from '@angular/core';
import { BookStoreMainComponent } from './book-store-main-form/book-store-main.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { Ng5SliderModule } from 'ng5-slider';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule, TooltipModule, PopoverModule, ButtonsModule } from 'angular-bootstrap-md';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

const appRoutes: Routes = [
  { path: '', component: BookStoreMainComponent }
];

@NgModule({
  declarations: [
    BookStoreMainComponent,
  ],
  imports: [
    NgSelectModule,
    ReactiveFormsModule,
    NgbModule,
    ModalModule.forRoot(),
    TooltipModule,
    BsDatepickerModule,
    PopoverModule,
    ButtonsModule,
    Ng5SliderModule,
    CommonModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserAnimationsModule,
  ],
  exports: [RouterModule,
    BookStoreMainComponent],
  bootstrap: [BookStoreMainComponent],


})
export class BookStoreModule { }
