export class GetAllGenresView
{
    constructor()
    {
        this.genres = [];
    }
    genres : GenreGetAllGenresViewItem[]
}
export class GenreGetAllGenresViewItem
{
    id : string;
    bookGenre : string; 
}