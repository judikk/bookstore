export class RegistrationUserView {
    userName: string;
    userSurname: string;
    userSex: string;
    email: string;
    userBirthday: "Date";
    userPhone: string;
    password: string;
    confirmPassword: string;
}