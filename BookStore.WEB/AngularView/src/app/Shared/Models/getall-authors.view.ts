export class GetAllAuthorsView {
    public authors: BookGetAllAuthorsViewItem[];
    constructor() {
        this.authors = [];
    }

}
export class BookGetAllAuthorsViewItem {
    id: string;
    authorName: string;
}