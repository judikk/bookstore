export class UpdateBookView {
    public bookDate: string;
    public enabled: boolean;
    public id: string;
    public bookName: string;
    public bookPrice: number;
    public allBookAuthors: string[];
    public allBookMagazines: string[];
    public allBookGenres: string[];


    constructor() {
        this.allBookAuthors = [];
        this.allBookMagazines = [];
        this.allBookGenres = [];
    }
}
