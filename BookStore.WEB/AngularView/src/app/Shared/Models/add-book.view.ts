export class AddBookView{
public constructor()
        {
            this.authorsID = [];
            this.magazinesID = [];
            this.genresID = [];
        }
        public bookName : string;
        public bookPrice : number; 
        public bookDate : Date;
        public authorsID : string[];
        public magazinesID : string[];
        public genresID : string[];
    }