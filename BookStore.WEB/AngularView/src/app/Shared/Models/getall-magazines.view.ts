export class GetAllMagazinesView
    {
        public constructor()
        {
            this.magazines = [];
        }
        public magazines : MagazineGetAllMagazinesViewItem[];
    }
    export class MagazineGetAllMagazinesViewItem
    {
        public id : string;
        public magazineName  : string; 
        public magazinePrice : number;
    }