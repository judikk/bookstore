export class FilterBookView
{
    public constructor() 
    {
        this.filterBooks = [];
        this.authorID = [];
        this.genreID =  [];
        this.magazineID =  [];
    }
    public filterBooks : BookFilterBookViewItem[]
    public authorID : string[];
    public genreID : string[];
    public magazineID : string[];
    public StartPrice : number; 
    public EndPrice : number;  
    public minDate : Date;
    public maxDate : Date;
    public searchString : string;
}
export class BookFilterBookViewItem
{               
    public bookDate : string;
    public id : string;  
    public bookName : string;  
    public bookPrice : number;  

}
