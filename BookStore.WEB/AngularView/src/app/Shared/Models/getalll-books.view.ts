import { Options } from 'ng5-slider';

export class GetAllBooksView {
    public books: BookGetAllBooksViewItem[];
    constructor() {
        this.books = [];
    }

}

export class BookGetAllBooksViewItem {
    public bookDate : string;
    public enabled : boolean;
    public id : string;  
    public bookName : string; 
    public bookPrice : number; 
    public allBookAuthors: GetAllAuthorsViewItem[];
    public allBookMagazines : GetAllMagazinesViewItem[];
    public allBookGenres : GetAllGenresViewItem[];

    
constructor()
{
    this.allBookAuthors = [];
    this.allBookMagazines = [];
    this.allBookGenres = [];
}
}


export class GetAllAuthorsViewItem {
    id: string 
    authorName : string  
}


export class GetAllMagazinesViewItem {
    id : string; 
    magazineName : string;
    magazinePrice : number;
}


export class GetAllGenresViewItem {
    id : string; 
    bookGenre : string;
}