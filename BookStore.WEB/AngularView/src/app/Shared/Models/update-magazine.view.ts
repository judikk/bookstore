export class UpdateMagazineView {
    public id: string;
    public magazineName: string;
    public magazinePrice: number;
}