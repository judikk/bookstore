﻿namespace BookStore.DAL
{
    public interface IOptionsDAL
    {
        string ConnectionString { get; }
    }
}
