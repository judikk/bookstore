﻿using BookStore.DAL.Context;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.ClassRepository;
using BookStore.DAL.Repositories.InterfaseRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace BookStore.DAL
{
    public class Startup
    {
        public static UserRepository _userRepository;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public static void ConfigureServices(IServiceCollection services, string connectionString)
        {

            services.AddDbContext<BookStoreContext>(opts => opts.UseSqlServer(connectionString, o => o.MigrationsAssembly("BookStore.DAL")));
            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IGenreRepository, GenreRepository>();
            services.AddScoped<IBooksGenresRepository, BooksGenresRepository>();
            services.AddScoped<IMagazineRepository, MagazineRepository>();
            services.AddScoped<IBooksAuthorsRepository, BooksAuthorsRepository>();
            services.AddScoped<IBooksMagazinesRepository, BooksMagazinesRepository>();
            services.AddScoped<IOrdersMagazinesRepository, OrdersMagazinesRepository>();
            services.AddScoped<IOrdersBooksRepository, OrdersBooksRepository>();
            services.AddIdentity<User, IdentityRole>(options =>
            {
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = new TimeSpan(0, 10, 0);
                options.Lockout.MaxFailedAccessAttempts = 5;
            }).AddEntityFrameworkStores<BookStoreContext>().AddDefaultTokenProviders();
        }

        private static async Task CreateUserRoles(IApplicationBuilder app)
        {

            using (IServiceScope scope = app.ApplicationServices.CreateScope())
            {
                RoleManager<IdentityRole> roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var roleCheck = await roleManager.RoleExistsAsync("admin");
                if (!roleCheck)
                {
                    await roleManager.CreateAsync(new IdentityRole("admin"));
                }
                roleCheck = await roleManager.RoleExistsAsync("user");
                if (!roleCheck)
                {
                    await roleManager.CreateAsync(new IdentityRole("user"));
                }
            }
            var user = new User()
            {
                UserPhone = "1",
                EmailConfirmed = true,
                UserBirthday = new DateTime(2000, 5, 5),
                UserSex = "mail",
                UserName = "admin",
                Email = "InkognitoZum@gmail.com"
            };
            using (IServiceScope scope = app.ApplicationServices.CreateScope())
            {
                UserManager<User> userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                var result = await userManager.CreateAsync(user, "K_o_z_a_r20005");
                if (result.Succeeded)
                    await userManager.AddToRoleAsync(user, "admin");
            }
        }

        public static void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            CreateUserRoles(app).Wait();
        }
    }
}
