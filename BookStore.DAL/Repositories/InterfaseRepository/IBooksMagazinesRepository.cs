﻿using BookStore.DAL.Entities;

namespace BookStore.DAL.Repositories.InterfaseRepository
{
    public interface IBooksMagazinesRepository : IBaseRepository<BooksMagazines>
    {

    }
}
