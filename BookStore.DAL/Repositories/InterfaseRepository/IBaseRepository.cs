﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.InterfaseRepository
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> Get(string id);
        Task Add(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
    }
}
