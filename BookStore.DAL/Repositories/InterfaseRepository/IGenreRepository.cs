﻿using BookStore.DAL.Entities;

namespace BookStore.DAL.Repositories.InterfaseRepository
{
    public interface IGenreRepository : IBaseRepository<Genre>
    {
    }
}
