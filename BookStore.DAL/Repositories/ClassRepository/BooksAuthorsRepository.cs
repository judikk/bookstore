﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class BooksAuthorsRepository : BaseRepository<BooksAuthors>, IBooksAuthorsRepository
    {
        readonly static string _tableName = "BooksAuthors";
        public BooksAuthorsRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
