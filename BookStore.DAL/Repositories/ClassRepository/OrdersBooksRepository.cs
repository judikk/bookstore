﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    class OrdersBooksRepository : BaseRepository<OrdersBooks>, IOrdersBooksRepository
    {
        readonly static string _tableName = "OrdersBooks";
        public OrdersBooksRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {

        }
    }
}
