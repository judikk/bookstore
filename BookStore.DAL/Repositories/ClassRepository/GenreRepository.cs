﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    class GenreRepository : BaseRepository<Genre>, IGenreRepository
    {
        readonly static string _tableName = "Genres";
        public GenreRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {

        }
    }
}
