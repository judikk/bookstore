﻿
using BookStore.DAL.Repositories.InterfaseRepository;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class BaseRepository<TEntity>
       : IBaseRepository<TEntity> where TEntity : class
    {
        readonly string ConnectionString;
        string _tableName { get; set; }
        public BaseRepository(IOptionsDAL optionsDAL)
        {
            ConnectionString = optionsDAL.ConnectionString;
        }

        public BaseRepository(string tableName, string connectionString)
        {
            ConnectionString = connectionString;
            _tableName = tableName;
        }


        public async Task Add(TEntity model)
        {

            using (var db = new SqlConnection(ConnectionString))
            {
                await db.InsertAsync(model);
            }
        }

        public async Task Delete(TEntity model)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                try{
                    await db.DeleteAsync(model);
                }
                catch(Exception ex)
                {

                }
                
            }
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                var sql = "SELECT * From " + _tableName;
                return (await db.QueryAsync<TEntity>(sql)).ToList();
            }
        }

        public async Task Update(TEntity model)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                await db.UpdateAsync<TEntity>(model);
            }
        }

        public async Task<TEntity> Get(string id)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                return (await db.QueryAsync<TEntity>("SELECT * FROM " + _tableName + " WHERE Id = @id", new { id })).FirstOrDefault();
            }
        }

    }
}
