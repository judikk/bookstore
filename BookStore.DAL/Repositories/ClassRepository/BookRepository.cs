﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        readonly static string _tableName = "Books";
        public BookRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
