﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    class OrdersMagazinesRepository : BaseRepository<OrdersMagazines>, IOrdersMagazinesRepository
    {
        readonly static string _tableName = "OrdersMagazines";
        public OrdersMagazinesRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {

        }
    }
}
