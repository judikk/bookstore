﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        readonly static string _tableName = "AspNetUsers";
        public UserRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
