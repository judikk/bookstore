﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        readonly static string _tableName = "Authors";
        public AuthorRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {

        }
    }
}
