﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class BooksMagazinesRepository : BaseRepository<BooksMagazines>, IBooksMagazinesRepository
    {
        readonly static string _tableName = "BooksMagazines";
        public BooksMagazinesRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
