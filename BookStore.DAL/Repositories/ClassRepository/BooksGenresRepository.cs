﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    class BooksGenresRepository : BaseRepository<BooksGenres>, IBooksGenresRepository
    {
        readonly static string _tableName = "BooksGenres";
        public BooksGenresRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {

        }
    }
}
