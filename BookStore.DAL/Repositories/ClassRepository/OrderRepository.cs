﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        readonly static string _tableName = "Orders";
        public OrderRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
