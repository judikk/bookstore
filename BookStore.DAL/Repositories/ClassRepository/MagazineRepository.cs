﻿using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;

namespace BookStore.DAL.Repositories.ClassRepository
{
    public class MagazineRepository : BaseRepository<Magazine>, IMagazineRepository
    {
        readonly static string _tableName = "Magazines";
        public MagazineRepository(IOptionsDAL options) : base(_tableName, options.ConnectionString)
        {
        }
    }
}
