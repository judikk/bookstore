﻿namespace BookStore.DAL.Entities
{
    public class Author : BaseEntity
    {
        public Author() : base()
        {

        }
        public string AuthorName { get; set; }
    }
}
