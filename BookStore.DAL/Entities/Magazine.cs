﻿namespace BookStore.DAL.Entities
{
    public class Magazine : BaseEntity
    {
        public Magazine() : base()
        {

        }
        public string MagazineName { get; set; }
        public int MagazinePrice { get; set; }

    }
}
