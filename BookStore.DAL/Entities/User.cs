﻿using Microsoft.AspNetCore.Identity;
using System;

namespace BookStore.DAL.Entities
{
    public class User : IdentityUser
    {
        public DateTime CreationDate { get; set; }
        public override string UserName { get; set; }
        public string UserSurname { get; set; }
        public string UserSex { get; set; }
        public override string Email { get; set; }
        public DateTime UserBirthday { get; set; }
        public string UserPhone { get; set; }
    }
}
