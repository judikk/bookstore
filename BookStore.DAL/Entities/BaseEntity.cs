﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DAL.Entities
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.UtcNow;
        }

        [ExplicitKey]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
