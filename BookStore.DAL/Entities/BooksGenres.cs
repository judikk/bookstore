﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    [Table("BooksGenres")]
    public class BooksGenres : BaseEntity
    {
        public BooksGenres() : base()
        {

        }
        public string BookID { get; set; }
        public string GenreID { get; set; }

        [Write(false)]
        public Book Book { get; set; }
        [Write(false)]
        public Genre Genre { get; set; }
    }
}
