﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class OrdersBooks : BaseEntity
    {
        public OrdersBooks() : base()
        {

        }
        public string OrderID { get; set; }
        public string BookID { get; set; }

        [Write(false)]
        public virtual Order Order { get; set; }
        [Write(false)]
        public virtual Book Book { get; set; }
    }
}
