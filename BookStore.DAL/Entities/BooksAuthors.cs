﻿
using Dapper.Contrib.Extensions;


namespace BookStore.DAL.Entities
{
    [Table("BooksAuthors")]
    public class BooksAuthors : BaseEntity
    {

        public BooksAuthors() : base()
        {

        }
        public string BookID { get; set; }
        public string AuthorID { get; set; }

        [Write(false)]
        public virtual Author Author { get; set; }
        [Write(false)]
        public virtual Book Book { get; set; }
    }
}
