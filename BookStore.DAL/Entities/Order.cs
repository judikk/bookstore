﻿
using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class Order : BaseEntity
    {
        public Order() : base()
        {

        }
        public string UserId { get; set; }

        [Write(false)]
        public virtual User User { get; set; }
    }
}
