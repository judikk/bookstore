﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class OrdersMagazines : BaseEntity
    {
        public OrdersMagazines() : base()
        {

        }
        public string OrderID { get; set; }
        public string MagazineID { get; set; }

        [Write(false)]
        public virtual Order Order { get; set; }
        [Write(false)]
        public virtual Magazine Magazine { get; set; }
    }
}
