﻿using Microsoft.AspNetCore.Identity;

namespace BookStore.DAL.Entities
{
    class Role : IdentityRole
    {
        public string RoleName { get; set; }
    }
}
