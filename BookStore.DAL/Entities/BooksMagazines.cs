﻿
using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    [Table("BooksMagazines")]
    public class BooksMagazines : BaseEntity
    {

        public BooksMagazines() : base()
        {

        }
        public string BookID { get; set; }
        public string MagazineID { get; set; }

        [Write(false)]
        public virtual Book Book { get; set; }
        [Write(false)]
        public virtual Magazine Magazin { get; set; }
    }
}
