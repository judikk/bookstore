﻿namespace BookStore.DAL.Entities
{
    public class Genre : BaseEntity
    {
        public Genre() : base()
        {

        }
        public string BookGenre { get; set; }
    }
}
