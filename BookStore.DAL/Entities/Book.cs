﻿using System;

namespace BookStore.DAL.Entities
{

    public class Book : BaseEntity
    {
        public Book() : base()
        {

        }
        public string BookName { get; set; }
        public int BookPrice { get; set; }
        public DateTime BookDate { get; set; }
    }
}
