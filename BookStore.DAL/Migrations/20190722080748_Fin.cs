﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.DAL.Migrations
{
    public partial class Fin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OrderID",
                table: "OrdersMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MagazineID",
                table: "OrdersMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OrderID",
                table: "OrdersBooks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "OrdersBooks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Orders",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MagazineID",
                table: "BooksMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GenreID",
                table: "BooksGenres",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksGenres",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksAuthors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AuthorID",
                table: "BooksAuthors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BookDate",
                table: "Books",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_OrdersMagazines_MagazineID",
                table: "OrdersMagazines",
                column: "MagazineID");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersMagazines_OrderID",
                table: "OrdersMagazines",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersBooks_BookID",
                table: "OrdersBooks",
                column: "BookID");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersBooks_OrderID",
                table: "OrdersBooks",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_UserId",
                table: "Orders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BooksMagazines_BookID",
                table: "BooksMagazines",
                column: "BookID");

            migrationBuilder.CreateIndex(
                name: "IX_BooksMagazines_MagazineID",
                table: "BooksMagazines",
                column: "MagazineID");

            migrationBuilder.CreateIndex(
                name: "IX_BooksGenres_BookID",
                table: "BooksGenres",
                column: "BookID");

            migrationBuilder.CreateIndex(
                name: "IX_BooksGenres_GenreID",
                table: "BooksGenres",
                column: "GenreID");

            migrationBuilder.CreateIndex(
                name: "IX_BooksAuthors_AuthorID",
                table: "BooksAuthors",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_BooksAuthors_BookID",
                table: "BooksAuthors",
                column: "BookID");

            migrationBuilder.AddForeignKey(
                name: "FK_BooksAuthors_Authors_AuthorID",
                table: "BooksAuthors",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BooksAuthors_Books_BookID",
                table: "BooksAuthors",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BooksGenres_Books_BookID",
                table: "BooksGenres",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BooksGenres_Genres_GenreID",
                table: "BooksGenres",
                column: "GenreID",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BooksMagazines_Books_BookID",
                table: "BooksMagazines",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BooksMagazines_Magazines_MagazineID",
                table: "BooksMagazines",
                column: "MagazineID",
                principalTable: "Magazines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersBooks_Books_BookID",
                table: "OrdersBooks",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersBooks_Orders_OrderID",
                table: "OrdersBooks",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersMagazines_Magazines_MagazineID",
                table: "OrdersMagazines",
                column: "MagazineID",
                principalTable: "Magazines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersMagazines_Orders_OrderID",
                table: "OrdersMagazines",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BooksAuthors_Authors_AuthorID",
                table: "BooksAuthors");

            migrationBuilder.DropForeignKey(
                name: "FK_BooksAuthors_Books_BookID",
                table: "BooksAuthors");

            migrationBuilder.DropForeignKey(
                name: "FK_BooksGenres_Books_BookID",
                table: "BooksGenres");

            migrationBuilder.DropForeignKey(
                name: "FK_BooksGenres_Genres_GenreID",
                table: "BooksGenres");

            migrationBuilder.DropForeignKey(
                name: "FK_BooksMagazines_Books_BookID",
                table: "BooksMagazines");

            migrationBuilder.DropForeignKey(
                name: "FK_BooksMagazines_Magazines_MagazineID",
                table: "BooksMagazines");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_OrdersBooks_Books_BookID",
                table: "OrdersBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_OrdersBooks_Orders_OrderID",
                table: "OrdersBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_OrdersMagazines_Magazines_MagazineID",
                table: "OrdersMagazines");

            migrationBuilder.DropForeignKey(
                name: "FK_OrdersMagazines_Orders_OrderID",
                table: "OrdersMagazines");

            migrationBuilder.DropIndex(
                name: "IX_OrdersMagazines_MagazineID",
                table: "OrdersMagazines");

            migrationBuilder.DropIndex(
                name: "IX_OrdersMagazines_OrderID",
                table: "OrdersMagazines");

            migrationBuilder.DropIndex(
                name: "IX_OrdersBooks_BookID",
                table: "OrdersBooks");

            migrationBuilder.DropIndex(
                name: "IX_OrdersBooks_OrderID",
                table: "OrdersBooks");

            migrationBuilder.DropIndex(
                name: "IX_Orders_UserId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_BooksMagazines_BookID",
                table: "BooksMagazines");

            migrationBuilder.DropIndex(
                name: "IX_BooksMagazines_MagazineID",
                table: "BooksMagazines");

            migrationBuilder.DropIndex(
                name: "IX_BooksGenres_BookID",
                table: "BooksGenres");

            migrationBuilder.DropIndex(
                name: "IX_BooksGenres_GenreID",
                table: "BooksGenres");

            migrationBuilder.DropIndex(
                name: "IX_BooksAuthors_AuthorID",
                table: "BooksAuthors");

            migrationBuilder.DropIndex(
                name: "IX_BooksAuthors_BookID",
                table: "BooksAuthors");

            migrationBuilder.DropColumn(
                name: "BookDate",
                table: "Books");

            migrationBuilder.AlterColumn<string>(
                name: "OrderID",
                table: "OrdersMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MagazineID",
                table: "OrdersMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OrderID",
                table: "OrdersBooks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "OrdersBooks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Orders",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MagazineID",
                table: "BooksMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksMagazines",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GenreID",
                table: "BooksGenres",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksGenres",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "BooksAuthors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AuthorID",
                table: "BooksAuthors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
