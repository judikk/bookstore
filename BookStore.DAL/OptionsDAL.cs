﻿namespace BookStore.DAL
{
    public class OptionsDAL : IOptionsDAL
    {
        public string ConnectionString { get; }
        public OptionsDAL(string connectionString)
        {
            ConnectionString = connectionString;
        }
    }
}
