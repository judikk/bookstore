﻿using BookStore.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DAL.Context
{
    public class BookStoreContext : IdentityDbContext<User>
    {
        public BookStoreContext(DbContextOptions<BookStoreContext> options) : base(options) => Database.EnsureCreated();
        public DbSet<Order> Orders { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<BooksAuthors> BooksAuthors { get; set; }
        public DbSet<BooksMagazines> BooksMagazines { get; set; }
        public DbSet<BooksGenres> BooksGenres { get; set; }
        public DbSet<OrdersBooks> OrdersBooks { get; set; }
        public DbSet<OrdersMagazines> OrdersMagazines { get; set; }

    }
}
