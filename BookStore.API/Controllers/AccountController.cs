﻿using BookStore.BLL.Services.Helpers;
using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]

    public class AccountController : Controller
    {
        private IAccountService _userService;

        public AccountController(IAccountService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateRefreshToken([FromBody] CreateRefreshTokenView model)
        {
            if (ModelState.IsValid)
                return Ok(await _userService.CreateRefreshToken(model));
            else
                return BadRequest(ModelState);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterUserView model)
        {
            if (ModelState.IsValid)
            {
                var _token = await _userService.RegisterEnabled(model);
                if (_token != null)
                {
                    var callbackUrl = Url.Action("ConfirmEmail",
                                                     "Account",
                                                     new
                                                     {
                                                         token = _token,
                                                         userName = model.UserName
                                                     },
                                                     protocol: Request.Scheme);
                    await _userService.SendEmailAsync(model.Email,
                                                      "Confirm your account",
                                                      $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>",
                                                      model.UserName);
                    return Ok();
                }
                else
                {
                    return BadRequest("Sorry:c This username already logged in!");
                }
            }
            return BadRequest("Registration failed");
        }


        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string token, string userName)
        {
            if (await _userService.ConfirmEmail(token, userName))
            {
                return Ok(token);
            }
            else
            {
                return BadRequest("Error");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> LogIn([FromBody] LoginUserView model)
        {

            if (!ModelState.IsValid) {
                return BadRequest("Login is failed");
            }
            var result = await _userService.LogInEnabled(model);
            if (result != null)
            {
                return Ok(await _userService.LogInEnabled(model));
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
