﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.MagazineService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MagazineController : Controller
    {
        private IMagazineService _magazineService;

        public MagazineController(IMagazineService magazineService)
        {
            _magazineService = magazineService;
        }

        [HttpGet]
        [Authorize(Roles = "user,admin")]
        public async Task<IActionResult> Get([FromBody] GetMagazineView model) => Ok(await _magazineService.Get(model.Id));

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll() => Ok(await _magazineService.GetAll());

        [HttpPost]
        [Authorize(Roles = "user,admin")]
        public async Task<IActionResult> Add([FromBody] AddMagazineView model)
        {
            await _magazineService.Add(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateMagazineView model)
        {
            await _magazineService.Update(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromBody] DeleteMagazineView model)
        {
            await _magazineService.Delete(model);
            return Ok(model);
        }

    }
}