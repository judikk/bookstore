﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.AuthorService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthorController : Controller
    {
        private IAuthorService _authorService;

        public AuthorController(IAuthorService authorBussinessViewModel)
        {
            _authorService = authorBussinessViewModel;
        }

        [HttpGet]
        [Authorize(Roles = "user,admin")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromBody] GetAuthorView model) => Ok(await _authorService.Get(model.Id));

        [HttpGet]
        [AllowAnonymous]

        public async Task<IActionResult> GetAll() => Ok(await _authorService.GetAll());

        [HttpPost]
        [Authorize(Roles = "admin")]
        // [AllowAnonymous]
        public async Task<IActionResult> Add([FromBody] AddAuthorView model)
        {
            await _authorService.Add(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromBody] DeleteAuthorView model)
        {
            await _authorService.Delete(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateAuthorView model)
        {
            await _authorService.Update(model);
            return Ok(model);
        }

    }
}