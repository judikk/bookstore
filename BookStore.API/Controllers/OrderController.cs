﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.OrderService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : Controller
    {
        private IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        [Authorize(Roles = "user,admin")]
        public async Task<IActionResult> Get([FromBody] GetOrderView model) => Ok(await _orderService.Get(model.Id));

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll() => Ok(await _orderService.GetAll());

        [HttpPost]
        [Authorize(Roles = "user,admin")]
        public async Task<IActionResult> Add([FromBody] AddOrderView model)
        {
            await _orderService.Add(model);
            return Ok(model);
        }

        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromBody] DeleteOrderView model)
        {
            await _orderService.Delete(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateOrderView model)
        {
            await _orderService.Update(model);
            return Ok(model);
        }
    }
}