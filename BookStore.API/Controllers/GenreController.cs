﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.GenreView;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GenreController : Controller
    {
        IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        [Authorize(Roles = "user,admin")]
        public async Task<IActionResult> Get([FromBody] GetGenreView model) => Ok(await _genreService.Get(model.Id));

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll() => Ok(await _genreService.GetAll());

        [HttpPost]
        //[Authorize(Roles = "admin")]
        [AllowAnonymous]
        public async Task<IActionResult> Add([FromBody] AddGenreView model)
        {
            await _genreService.Add(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromBody] DeleteGenreView model)
        {
            await _genreService.Delete(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateGenreView model)
        {
            await _genreService.Update(model);
            return Ok(model);
        }

    }
}