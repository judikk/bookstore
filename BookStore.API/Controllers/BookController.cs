﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.BookAndAuthorService;
using BookStore.BLL.Views.BookService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BookController : Controller
    {
        private IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromBody] GetBookView model) => Ok(await _bookService.Get(model.Id));

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _bookService.GetAll());
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Add([FromBody] AddBookView model)
        {
            await _bookService.Add(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromBody] DeleteBookView model)
        {
            await _bookService.Delete(model);
            return Ok(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateBookView model)
        {
            await _bookService.Update(model);
            return Ok(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Filter([FromBody] FilterBookView model) => Ok(await _bookService.Filter(model));

        
    }
}