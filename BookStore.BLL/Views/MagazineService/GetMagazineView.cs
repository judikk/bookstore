﻿namespace BookStore.BLL.Views.MagazineService
{
    public class GetMagazineView
    {
        public string Id { get; set; }
        public int MagazinePrice { get; set; }
        public string MagazineName { get; set; }
    }
}
