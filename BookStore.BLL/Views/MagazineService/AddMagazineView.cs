﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.MagazineService
{
    public class AddMagazineView
    {
        [Required]
        public string MagazineName { get; set; }
        [Required]
        public int MagazinePrice { get; set; }
    }
}
