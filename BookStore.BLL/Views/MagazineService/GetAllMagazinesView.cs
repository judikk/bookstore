﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.MagazineService
{
    public class GetAllMagazinesView
    {
        public GetAllMagazinesView()
        {
            Magazines = new List<MagazineGetAllMagazinesViewItem>();
        }
        public List<MagazineGetAllMagazinesViewItem> Magazines { get; set; }
    }
    public class MagazineGetAllMagazinesViewItem
    {
        public string Id { get; set; }
        public string MagazineName { get; set; }
        public int MagazinePrice { get; set; }
    }
}
