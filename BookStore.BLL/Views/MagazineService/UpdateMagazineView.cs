﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.MagazineService
{
    public class UpdateMagazineView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string MagazineName { get; set; }
        [Required]
        public int MagazinePrice { get; set; }
    }
}
