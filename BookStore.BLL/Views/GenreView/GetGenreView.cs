﻿namespace BookStore.BLL.Views.GenreView
{
    public class GetGenreView
    {
        public string Id { get; set; }
        public string BookGenre { get; set; }
    }
}
