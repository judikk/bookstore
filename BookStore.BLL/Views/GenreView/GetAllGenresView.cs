﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.GenreView
{
    public class GetAllGenresView
    {
        public GetAllGenresView()
        {
            Genres = new List<GenreGetAllGenresViewItem>();
        }
        public List<GenreGetAllGenresViewItem> Genres { get; set; }
    }
    public class GenreGetAllGenresViewItem
    {
        public string Id { get; set; }
        public string BookGenre { get; set; }
    }
}
