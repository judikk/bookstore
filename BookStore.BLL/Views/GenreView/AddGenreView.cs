﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.GenreView
{
    public class AddGenreView
    {
        [Required]
        public string BookGenre { get; set; }
    }
}
