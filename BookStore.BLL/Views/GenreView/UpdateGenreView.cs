﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.GenreView
{
    public class UpdateGenreView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string BookGenre { get; set; }
    }
}
