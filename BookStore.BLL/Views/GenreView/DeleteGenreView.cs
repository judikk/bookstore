﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.GenreView
{
    public class DeleteGenreView
    {
        [Required]
        public string Id { get; set; }
    }
}
