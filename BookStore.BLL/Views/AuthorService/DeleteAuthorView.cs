﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.AuthorService
{
    public class DeleteAuthorView
    {
        [Required]
        public string Id { get; set; }
    }
}
