﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.AuthorService
{
    public class GetAllAuthorsView
    {
        public GetAllAuthorsView()
        {
            Authors = new List<AuthorGetAllAuthorsViewItem>();
        }
        public List<AuthorGetAllAuthorsViewItem> Authors { get; set; }
    }
    public class AuthorGetAllAuthorsViewItem
    {
        public string Id { get; set; }
        public string AuthorName { get; set; }
    }

}
