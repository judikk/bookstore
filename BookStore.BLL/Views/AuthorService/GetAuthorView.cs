﻿namespace BookStore.BLL.Views.AuthorService
{
    public class GetAuthorView
    {
        public string Id { get; set; }
        public string AuthorName { get; set; }
    }
}
