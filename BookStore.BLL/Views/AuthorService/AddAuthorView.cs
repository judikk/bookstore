﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.AuthorService
{
    public class AddAuthorView
    {
        [Required]
        public string AuthorName { get; set; }
    }
}
