﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.AuthorService
{
    public class UpdateAuthorView
    {
        [Required]
        public string Id { get; set; }
        public string AuthorName { get; set; }
    }
}
