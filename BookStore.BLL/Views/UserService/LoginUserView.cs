﻿namespace BookStore.BLL.Views.UserService
{
    public class LoginUserView
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
