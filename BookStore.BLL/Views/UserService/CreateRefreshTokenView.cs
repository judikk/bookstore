﻿namespace BookStore.BLL.Views.UserService
{
    public class CreateRefreshTokenView
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
