﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.UserService
{
    public class RegisterUserView
    {
        [Required]
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string UserSex { get; set; }
        [Required]
        public string Email { get; set; }
        public DateTime UserBirthday { get; set; }
        public string UserPhone { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
