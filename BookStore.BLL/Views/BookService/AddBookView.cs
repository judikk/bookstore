﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService
{
    public class AddBookView
    {
        public AddBookView()
        {
            AuthorsID = new List<string>();
            MagazinesID = new List<string>();
            GenresID = new List<string>();
        }
        [Required]
        public string BookName { get; set; }
        [Required]
        public int BookPrice { get; set; }
        [Required]
        public DateTime BookDate { get; set; }
        [Required]
        public List<string> AuthorsID { get; set; }
        [Required]
        public List<string> MagazinesID { get; set; }
        [Required]
        public List<string> GenresID { get; set; }
    }
}
