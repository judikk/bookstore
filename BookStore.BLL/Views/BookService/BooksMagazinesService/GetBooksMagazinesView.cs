﻿namespace BookStore.BLL.Views.BookService.BooksMagazinesService
{
    public class GetBooksMagazinesView
    {
        public string Id { get; set; }
        public string BookID { get; set; }
        public string MagazineID { get; set; }
    }
}
