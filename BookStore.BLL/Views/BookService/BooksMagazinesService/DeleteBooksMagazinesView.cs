﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksMagazinesService
{
    public class DeleteBooksMagazinesView
    {
        [Required]
        public string Id { get; set; }
    }
}
