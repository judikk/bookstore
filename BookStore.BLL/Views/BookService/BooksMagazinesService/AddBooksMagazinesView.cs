﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksMagazinesService
{
    public class AddBooksMagazinesView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string BookID { get; set; }
        [Required]
        public string MagazineID { get; set; }
    }
}
