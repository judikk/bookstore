﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.BookService.BooksMagazinesService
{
    public class GetAllBooksMagazinesView
    {
        public GetAllBooksMagazinesView()
        {
            BooksMagazinesList = new List<BooksMagazinesGetAllBooksMagazinesViewItem>();
        }
        public List<BooksMagazinesGetAllBooksMagazinesViewItem> BooksMagazinesList { get; set; }
    }
    public class BooksMagazinesGetAllBooksMagazinesViewItem
    {
        public string Id { get; set; }
        public string BookID { get; set; }
        public string MagazineID { get; set; }
    }
}
