﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksMagazinesService
{
    public class UpdateBooksMagazinesView
    {
        [Required]
        public string Id { get; set; }
        public string BookID { get; set; }
        public string MagazineID { get; set; }
    }
}
