﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksAuthorsService
{
    public class UpdateBookAndAuthorView
    {
        [Required]
        public string Id { get; set; }
        public string BookID { get; set; }
        public string AuthorID { get; set; }
    }
}
