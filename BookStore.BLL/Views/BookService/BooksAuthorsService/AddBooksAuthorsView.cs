﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksAuthorsService
{
    public class AddBooksAuthorsView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string AuthorID { get; set; }
        [Required]
        public string BookID { get; set; }
    }
}
