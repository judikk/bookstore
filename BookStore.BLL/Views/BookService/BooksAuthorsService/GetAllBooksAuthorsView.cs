﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.BookService.BooksAuthorsService
{
    public class GetAllBooksAuthorsView
    {
        public GetAllBooksAuthorsView()
        {
            BooksAuthorsList = new List<BooksAuthorsGetAllBooksAuthorsViewItem>();
        }
        public List<BooksAuthorsGetAllBooksAuthorsViewItem> BooksAuthorsList { get; set; }
    }
    public class BooksAuthorsGetAllBooksAuthorsViewItem
    {
        public string Id { get; set; }
        public string AuthorID { get; set; }
        public string BookID { get; set; }
    }
}
