﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksAuthorsService
{
    public class DeleteBooksAuthorsView
    {
        [Required]
        public string Id { get; set; }
    }
}
