﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService
{
    public class DeleteBookView
    {
        [Required]
        public string Id { get; set; }
    }
}
