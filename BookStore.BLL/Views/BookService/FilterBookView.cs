﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views.BookService
{
    public class FilterBookView
    {
        public FilterBookView()
        {
            FilterBooks = new List<BookFilterBookViewItem>();
            AuthorID = new List<string>();
            GenreID = new List<string>();
            MagazineID = new List<string>();
        }
        public List<BookFilterBookViewItem> FilterBooks { get; set; }
        public List<string> AuthorID { get; set; }
        public List<string> GenreID { get; set; }
        public List<string> MagazineID { get; set; }
        public long StartPrice { get; set; }
        public long EndPrice { get; set; }
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public string SearchString { get; set; }
    }
    public class BookFilterBookViewItem
    {
        public DateTime BookDate { get; set; }
        public string Id { get; set; }
        public string BookName { get; set; }
        public int BookPrice { get; set; } 
    }


}
