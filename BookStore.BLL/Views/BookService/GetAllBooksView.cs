﻿
using BookStore.BLL.Views.GenreView;
using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views.BookService
{
 
        public class GetAllBooksView
        {
            public GetAllBooksView()
            {
                Books = new List<BookGetAllBooksViewItem>();
            }
            public List<BookGetAllBooksViewItem> Books { get; set; }
        }
        public class BookGetAllBooksViewItem
        {
        public BookGetAllBooksViewItem()
        {
            AllBookAuthors = new List<GetAllAuthorsViewItem>();
            AllBookMagazines = new List<GetAllMagazinesViewItem>();
            AllBookGenres = new List<GetAllGenresViewItem>();

        }
            public DateTime BookDate { get; set; }
            public string Id { get; set; }
            public string BookName { get; set; }
            public int BookPrice { get; set; }
            public List<GetAllAuthorsViewItem> AllBookAuthors { get; set; }
            public List<GetAllMagazinesViewItem> AllBookMagazines { get; set; }
            public List<GetAllGenresViewItem> AllBookGenres { get; set; }
        }
    public class GetAllAuthorsViewItem
    {
        public string Id { get; set; }
        public string AuthorName { get; set; }
    }
    public class GetAllMagazinesViewItem
    {
        public string Id { get; set; }
        public string MagazineName { get; set; }
        public int MagazinePrice { get; set; }
    }
    public class GetAllGenresViewItem
    {
        public string Id { get; set; }
        public string BookGenre { get; set; }
        public int BookDate { get; set; }
    }
}