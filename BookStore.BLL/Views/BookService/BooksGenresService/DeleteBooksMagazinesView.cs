﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksGenresService
{
    public class DeleteBooksGenresView
    {
        [Required]
        public string Id { get; set; }
    }
}
