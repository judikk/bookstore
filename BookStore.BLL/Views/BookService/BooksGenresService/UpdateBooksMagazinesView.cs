﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksGenresService
{
    public class UpdateBooksGenresView
    {
        [Required]
        public string Id { get; set; }
        public string BookID { get; set; }
        public string GenreID { get; set; }
    }
}
