﻿namespace BookStore.BLL.Views.BookService.BooksGenresService
{
    public class GetBooksGenresView
    {
        public string Id { get; set; }
        public string BookID { get; set; }
        public string GenreID { get; set; }
    }
}
