﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.BookService.BooksGenresService
{
    public class AddBooksGenresView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string BookID { get; set; }
        [Required]
        public string GenreID { get; set; }
    }
}
