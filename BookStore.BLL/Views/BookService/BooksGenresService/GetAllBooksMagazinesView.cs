﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.BookService.BooksGenresService
{
    public class GetAllBooksGenresView
    {
        public GetAllBooksGenresView()
        {
            BooksGenresList = new List<BooksGenresGetAllBooksGenresViewItem>();
        }
        public List<BooksGenresGetAllBooksGenresViewItem> BooksGenresList { get; set; }
    }
    public class BooksGenresGetAllBooksGenresViewItem
    {
        public string Id { get; set; }
        public string BookID { get; set; }
        public string GenreID { get; set; }
    }
}
