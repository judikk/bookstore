﻿using System;

namespace BookStore.BLL.Views.BookService
{
    public class GetBookView
    {
        public string Id { get; set; }
        public string BookName { get; set; }
        public int BookPrice { get; set; }
        public string AuthorID { get; set; }
        public string MagazineID { get; set; }
        public string GenreID { get; set; }
        public DateTime BookDate { get; set; }
    }
}
