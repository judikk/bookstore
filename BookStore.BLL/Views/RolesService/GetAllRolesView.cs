﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BookStore.BLL.Views.RolesService
{
    class GetAllRolesView
    {
        public List<IdentityRole> AllRoles { get; set; }
        public GetAllRolesView()
        {
            AllRoles = new List<IdentityRole>();
        }
    }
}
