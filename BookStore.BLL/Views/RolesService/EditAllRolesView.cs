﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.RolesService
{
    public class EditAllRolesView
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string UserEmail { get; set; }
        public List<RoleChangeRoleViewItem> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public EditAllRolesView()
        {
            AllRoles = new List<RoleChangeRoleViewItem>();
            UserRoles = new List<string>();
        }
    }
}
