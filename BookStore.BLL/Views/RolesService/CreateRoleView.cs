﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.RolesService
{
    public class CreateRoleView
    {
        [Required]
        public string RoleName { get; set; }
    }
}
