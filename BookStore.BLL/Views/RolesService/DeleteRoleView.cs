﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.RolesService
{
    public class DeleteRoleView
    {
        [Required]
        public string Id { get; set; }
    }
}
