﻿

using System.Collections.Generic;

namespace BookStore.BLL.Views.RolesService
{
    public class ChangeRoleView
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public List<RoleChangeRoleViewItem> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public ChangeRoleView()
        {
            AllRoles = new List<RoleChangeRoleViewItem>();
            UserRoles = new List<string>();
        }

    }

    public class RoleChangeRoleViewItem
    {
        public string RoleName { get; set; }
    }
}
