﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.RolesService
{
    public class EditRoleView
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string RoleName { get; set; }
    }
}
