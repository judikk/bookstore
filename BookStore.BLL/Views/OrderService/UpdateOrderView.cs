﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.OrderService
{
    public class UpdateOrderView
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string UserId { get; set; }
    }
}
