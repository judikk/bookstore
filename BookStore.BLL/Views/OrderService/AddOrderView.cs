﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.OrderService
{
    public class AddOrderView
    {
        [Required]
        public string UserId { get; set; }
    }
}
