﻿namespace BookStore.BLL.Views.OrderService
{
    public class GetOrderView
    {
        public string Id { get; set; }
        public string UserID { get; set; }
    }
}
