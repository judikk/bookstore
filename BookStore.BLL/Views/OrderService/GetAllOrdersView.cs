﻿using System.Collections.Generic;

namespace BookStore.BLL.Views.OrderService
{
    public class GetAllOrdersView
    {
        public GetAllOrdersView()
        {
            Orders = new List<OrderGetAllOrdersViewItem>();
        }
        public List<OrderGetAllOrdersViewItem> Orders { get; set; }
    }
    public class OrderGetAllOrdersViewItem
    {
        public string Id { get; set; }
        public string UserID { get; set; }
    }
}
