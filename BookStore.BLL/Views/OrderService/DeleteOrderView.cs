﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views.OrderService
{
    public class DeleteOrderView
    {
        [Required]
        public string Id { get; set; }
    }
}
