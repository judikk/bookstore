﻿using BookStore.BLL.Services.Helpers;
using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.UserService;
using BookStore.DAL.Entities;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public static IConfiguration Configuration { get; set; }
        public AccountService(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            Configuration = configuration;
        }

        public async Task<TokenService> CreateToken(LoginUserView user)
        {
            var identity = await GetIdentity(user.UserName);
            if (identity == null)
            {
                return null;
            }
            var now = DateTime.Now;
            // создаем JWT_access-токенa
            var jwt_access = new JwtSecurityToken(
                issuer: Configuration["TokenOptions:ISSURE"],
                audience: Configuration["TokenOptions:AUDIENCE"],
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(int.Parse(Configuration["TokenOptions:accessTokenTime"]))),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var access_token = new JwtSecurityTokenHandler().WriteToken(jwt_access);
            //----------------------
            // создаем JWT_refresh-токенa
            var jwt_refresh = new JwtSecurityToken(
                issuer: Configuration["TokenOptions:ISSURE"],
                audience: Configuration["TokenOptions:AUDIENCE"],
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(int.Parse(Configuration["TokenOptions:refreshTokenTime"]))),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var refresh_token = new JwtSecurityTokenHandler().WriteToken(jwt_refresh);
            //----------------------
            TokenService service = new TokenService()
            {
                AccessToken = access_token,
                RefreshToken = refresh_token
            };
            return service;
        }
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["TokenOptions:KEY"]));
        }

        public async Task SendEmailAsync(string email, string subject, string message, string userName)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Judik", "InkognitoZum@gmail.com"));
            emailMessage.To.Add(new MailboxAddress(userName, email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync("InkognitoZum@gmail.com", "K_o_z_a_r20005");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }

        public async Task<TokenService> LogInEnabled(LoginUserView model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                return null;
            }
            var result = await _signInManager.PasswordSignInAsync(
                user,
                model.Password,
                isPersistent: true,
                lockoutOnFailure: true);

            if (result.Succeeded)
            {
                var Tokens = await CreateToken(model);
                return Tokens;
            }
            else
                return null;
        }

        private async Task<ClaimsIdentity> GetIdentity(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var role = await _userManager.GetRolesAsync(user);
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimTypes.Role, role.FirstOrDefault()),
                    new Claim("confirmemail", user.EmailConfirmed.ToString()),
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims,
                                   "Token",
                                   ClaimsIdentity.DefaultNameClaimType,
                                   ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            // если пользователь не найден
            return null;
        }

        public async Task<string> RegisterEnabled(RegisterUserView model)
        {
            User user = new User()
            {
                CreationDate = DateTime.Now,
                UserSurname = model.UserSurname,
                Email = model.Email,
                UserBirthday = model.UserBirthday,
                UserName = model.UserName,
                UserPhone = model.UserPhone,
                UserSex = model.UserSex
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "user");
                var _code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                return _code;
            }
            else
                return null;
        }

        public async Task<CreateRefreshTokenView> CreateRefreshToken(CreateRefreshTokenView tokenService)
        {
            var accessTo = new JwtSecurityTokenHandler().ReadToken(tokenService.AccessToken) as JwtSecurityToken;
            if (tokenService.AccessToken == null)
                return null;
            var now = DateTime.UtcNow;
            // создаем JWT_access-токенa
            var jwt_access = new JwtSecurityToken(
               issuer: Configuration["TokenOptions:ISSURE"],
               //audience: Configuration["TokenOptions:AUDIENCE"],
               notBefore: now,
               claims: accessTo.Claims,
               expires: now.Add(TimeSpan.FromMinutes(int.Parse(Configuration["TokenOptions:accessTokenTime"]))),
               signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var access_token = new JwtSecurityTokenHandler().WriteToken(jwt_access);
            //----------------------
            // создаем JWT_refresh-токенa
            var jwt_refresh = new JwtSecurityToken(
                issuer: Configuration["TokenOptions:ISSURE"],
               //audience: Configuration["TokenOptions:AUDIENCE"],
                notBefore: now,
                claims: accessTo.Claims,
                expires: now.Add(TimeSpan.FromMinutes(int.Parse(Configuration["TokenOptions:refreshTokenTime"]))),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var refresh_token = new JwtSecurityTokenHandler().WriteToken(jwt_refresh);
            //----------------------
            CreateRefreshTokenView service = new CreateRefreshTokenView()
            {
                AccessToken = access_token,
                RefreshToken = refresh_token
            };
            return service;
        }

        public async Task<bool> ConfirmEmail(string token, string userName)
        {
            if (userName == null || token == null)
            {
                return false;
            }
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return false;
            }
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return true;
            }
            else
                return false;
        }
    }
}
