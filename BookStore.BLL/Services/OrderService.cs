﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.OrderService;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;
using System;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task Add(AddOrderView model)
        {
            var order = new Order()
            {
                UserId = model.UserId,
                CreationDate = DateTime.UtcNow
            };
            await _orderRepository.Add(order);
        }

        public async Task Delete(DeleteOrderView model)
        {
            var order = await _orderRepository.Get(model.Id);
            await _orderRepository.Delete(order);
        }

        public async Task<GetOrderView> Get(string id)
        {
            if (id == null)
            {
                throw new Exception("id is null");
            }
            var order = await _orderRepository.Get(id);
            var orderGetView = new GetOrderView()
            {
                Id = order.Id,
                UserID = order.UserId
            };
            return orderGetView;
        }

        public async Task<GetAllOrdersView> GetAll()
        {
            var orderList = await _orderRepository.GetAll();
            var orderGetViews = new GetAllOrdersView();
            foreach (Order order in orderList)
            {
                orderGetViews.Orders.Add(new OrderGetAllOrdersViewItem()
                {
                    Id = order.Id,
                    UserID = order.UserId
                }); ;
            }
            return orderGetViews;
        }

        public async Task Update(UpdateOrderView model)
        {
            var newOrder = await _orderRepository.Get(model.Id);
            newOrder.UserId = model.UserId;
            await _orderRepository.Update(newOrder);
        }
    }
}
