﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.BookService;
using BookStore.BLL.Views.BookService.BooksAuthorsService;
using BookStore.BLL.Views.BookService.BooksGenresService;
using BookStore.BLL.Views.BookService.BooksMagazinesService;
using BookStore.BLL.Views.MagazineService;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class BookService : IBookService
    {
        private IBookRepository _bookRepository;
        private IBooksGenresRepository _booksGenresRepository;
        private IBooksMagazinesRepository _booksMagazinesRepository;
        private IBooksAuthorsRepository _booksAuthorsRepository;
        private IMagazineRepository _magazineRepository;
        private IGenreRepository _genreRepository;
        private IAuthorRepository _authorRepository;
        public BookService(IBookRepository bookRepository, 
            IBooksMagazinesRepository booksMagazinesRepository, 
            IBooksGenresRepository booksGenresRepository, 
            IBooksAuthorsRepository booksAuthorsRepository,
            IMagazineRepository magazineRepository,
            IGenreRepository genreRepository,
            IAuthorRepository authorRepository)
        {
            _bookRepository = bookRepository;
            _booksAuthorsRepository = booksAuthorsRepository;
            _booksGenresRepository = booksGenresRepository;
            _booksMagazinesRepository = booksMagazinesRepository;
            _genreRepository = genreRepository;
            _magazineRepository = magazineRepository;
            _authorRepository = authorRepository;
        }

        public async Task Add(AddBookView model)
        {
            var book = new Book()
            {
                BookDate = model.BookDate,
                BookName = model.BookName,
                CreationDate = DateTime.UtcNow,
                BookPrice = model.BookPrice
            };

            await _bookRepository.Add(book);


            foreach (string Id in model.AuthorsID)
            {
                var booksAuthors = new BooksAuthors()
                {
                    AuthorID = Id,
                    BookID = book.Id,
                };
                await _booksAuthorsRepository.Add(booksAuthors);
            }
            foreach (string Id in model.MagazinesID)
            {
                var booksMagazines = new BooksMagazines()
                {
                    BookID = book.Id,
                    MagazineID = Id,
                };
                await _booksMagazinesRepository.Add(booksMagazines);
            }
            foreach (string Id in model.GenresID)
            {
                var booksGenres = new BooksGenres()
                {
                    BookID = book.Id,
                    GenreID = Id
                };
                await _booksGenresRepository.Add(booksGenres);
            }  
        }


        public async Task Delete(DeleteBookView model)
        {
            var book = await _bookRepository.Get(model.Id);
            var booksAuthors = await _booksAuthorsRepository.GetAll();
            var booksMagazines = await _booksMagazinesRepository.GetAll();
            var booksGenres = await _booksGenresRepository.GetAll();
            booksAuthors = booksAuthors.Where(x => x.BookID == model.Id ).ToList();
            booksMagazines = booksMagazines.Where(x => x.BookID == model.Id).ToList();
            booksGenres = booksGenres.Where(x => x.BookID == model.Id).ToList();
            
            foreach (BooksAuthors author in booksAuthors)
            {
                await _booksAuthorsRepository.Delete(author);
            }
            foreach (BooksMagazines magazines in booksMagazines)
            {
                await _booksMagazinesRepository.Delete(magazines);
            }
            foreach (BooksGenres genres in booksGenres)
            {
                await _booksGenresRepository.Delete(genres);
            }
            await _bookRepository.Delete(book);
        }

        public async Task<GetBookView> Get(string id)
        {
            if (id == null)
            {
                throw new Exception("id is null");
            }
            var book = await _bookRepository.Get(id);
            var authorGetView = new GetBookView()
            {
                BookDate = book.BookDate,
                Id = book.Id,
                BookName = book.BookName,
                BookPrice = book.BookPrice,
            };
            return authorGetView;
        }


        public async Task<GetAllBooksView> GetAll()
        {
            var bookList = await _bookRepository.GetAll();
            var bookGetViews = new GetAllBooksView();
            foreach (Book book in bookList)
            {
                bookGetViews.Books.Add(new BookGetAllBooksViewItem()
                {
                    BookDate = book.BookDate,
                    Id = book.Id,
                    BookName = book.BookName,
                    BookPrice = book.BookPrice,
                    AllBookAuthors = await GetAllBooksAuthors(book.Id),
                    AllBookGenres = await GetAllBooksGenres(book.Id),
                    AllBookMagazines = await GetAllBooksMagazines(book.Id),
                });
            }
            return bookGetViews;
        }

        public async Task Update(UpdateBookView model)
        {
            var newBook = await _bookRepository.Get(model.Id);
            newBook.BookDate = model.BookDate;
            newBook.BookName = model.BookName;
            newBook.BookPrice = model.BookPrice;

            foreach (string Id in model.AuthorsID)
            {
                var booksAuthors = await _booksAuthorsRepository.Get(Id);
                booksAuthors.AuthorID = Id;
                booksAuthors.BookID = model.Id;
                await _booksAuthorsRepository.Update(booksAuthors);
            }
            foreach (string Id in model.MagazinesID)
            {
                var booksMagazines = await _booksMagazinesRepository.Get(Id);
                booksMagazines.MagazineID = Id;
                booksMagazines.BookID = model.Id;
                await _booksMagazinesRepository.Update(booksMagazines);
            }
            foreach (string Id in model.GenresID)
            {
                var booksGenres = await _booksGenresRepository.Get(Id);
                booksGenres.GenreID = Id;
                booksGenres.BookID = model.Id;
                await _booksGenresRepository.Update(booksGenres);
            }
            await _bookRepository.Update(newBook);

        }

        public async Task<FilterBookView> Filter(FilterBookView model)
        {
            var books = await _bookRepository.GetAll();
            var booksAuthors = await _booksAuthorsRepository.GetAll();
            var booksGenres = await _booksGenresRepository.GetAll();
            var booksMagazines = await _booksMagazinesRepository.GetAll();

            if (model.SearchString != null)
                books = books.Where(x => x.BookName.Contains(model.SearchString)).ToList();

            if (model.StartPrice != 0 && model.EndPrice != 0)
                books = books.Where(x => x.BookPrice < model.EndPrice && x.BookPrice > model.StartPrice).ToList();

            if (model.AuthorID.Count != 0)
                books = books.Where(x => booksAuthors.Any(y => y.BookID == x.Id && null != model.AuthorID.FirstOrDefault(z => z == y.AuthorID))).ToList();

            if (model.GenreID.Count != 0)
                books = books.Where(x => booksGenres.Any(y => y.BookID == x.Id && null != model.GenreID.FirstOrDefault(z => z == y.GenreID))).ToList();

            if (model.MagazineID.Count != 0)
                books = books.Where(x => booksMagazines.Any(y => y.BookID == x.Id && null != model.MagazineID.FirstOrDefault(z => z == y.MagazineID))).ToList();

            if (!model.MaxDate.Equals(new DateTime()) && !model.MinDate.Equals(new DateTime()))
                books = books.Where(x => x.BookDate < model.MaxDate && x.BookDate > model.MinDate).ToList();

            var bookGetViews = new FilterBookView();
            foreach (Book book in books)
            {
                bookGetViews.FilterBooks.Add(new BookFilterBookViewItem()
                {
                    BookDate = book.BookDate,
                    Id = book.Id,
                    BookPrice = book.BookPrice,
                    BookName = book.BookName,

                }); ;
            }
            return bookGetViews;
        }

        public async Task<List<GetAllAuthorsViewItem>> GetAllBooksAuthors(string id)
        {
            var getAllBookAuthorsView = new GetAllBooksAuthorsView();
            var booksAuthorsList = await _booksAuthorsRepository.GetAll();
            foreach (BooksAuthors booksAuthors in booksAuthorsList)
            {
                getAllBookAuthorsView.BooksAuthorsList.Add(new BooksAuthorsGetAllBooksAuthorsViewItem()
                {
                    Id = booksAuthors.Id,
                    AuthorID = booksAuthors.AuthorID,
                    BookID = booksAuthors.BookID,
                }); ;
            }
            getAllBookAuthorsView.BooksAuthorsList = getAllBookAuthorsView.BooksAuthorsList.Where(x => x.BookID == id).ToList();
            var authorsList = await _authorRepository.GetAll();

            authorsList = authorsList.Where(x => null != getAllBookAuthorsView.BooksAuthorsList.FirstOrDefault(y => y.AuthorID == x.Id));

            var getAllAuthorsView = new List<GetAllAuthorsViewItem>();

            foreach (Author author in authorsList)
            {
                getAllAuthorsView.Add(new GetAllAuthorsViewItem()
                {
                    Id = author.Id,
                    AuthorName = author.AuthorName

                }); ;
            }
            return getAllAuthorsView;
        }

        public async Task<List<GetAllGenresViewItem>> GetAllBooksGenres(string id)
        {
            var getAllBookGenresView = new GetAllBooksGenresView();
            var booksGenresList = await _booksGenresRepository.GetAll();
            foreach (BooksGenres booksAuthors in booksGenresList)
            {
                getAllBookGenresView.BooksGenresList.Add(new BooksGenresGetAllBooksGenresViewItem()
                {
                    Id = booksAuthors.Id,
                    GenreID = booksAuthors.GenreID,
                    BookID = booksAuthors.BookID,
                }); ;
            }
            getAllBookGenresView.BooksGenresList = getAllBookGenresView.BooksGenresList.Where(x => x.BookID == id).ToList();
            var genresList = await _genreRepository.GetAll();

            genresList = genresList.Where(x => null != getAllBookGenresView.BooksGenresList.FirstOrDefault(y => y.GenreID == x.Id));

            var getAllGenresView = new List<GetAllGenresViewItem>();
            foreach (Genre genre in genresList)
            {
                getAllGenresView.Add(new GetAllGenresViewItem()
                {
                    Id = genre.Id,
                    BookGenre = genre.BookGenre,
                }); ;
            }
            return getAllGenresView;
        }

        public async Task<List<GetAllMagazinesViewItem>> GetAllBooksMagazines(string id)
        {
            var getAllBookMagazinesView = new GetAllBooksMagazinesView();
            var booksMagazinesList = await _booksMagazinesRepository.GetAll();
            foreach (BooksMagazines booksMagazines in booksMagazinesList)
            {
                getAllBookMagazinesView.BooksMagazinesList.Add(new BooksMagazinesGetAllBooksMagazinesViewItem()
                {
                    Id = booksMagazines.Id,
                    MagazineID = booksMagazines.MagazineID,
                    BookID = booksMagazines.BookID,
                }); ;
            }
            getAllBookMagazinesView.BooksMagazinesList = getAllBookMagazinesView.BooksMagazinesList.Where(x => x.BookID == id).ToList();
            var magazinesList = await _magazineRepository.GetAll();

            magazinesList = magazinesList.Where(x => null != getAllBookMagazinesView.BooksMagazinesList.FirstOrDefault(y => y.MagazineID == x.Id));

            var getAllMagazinesView = new List<GetAllMagazinesViewItem>();

            foreach (Magazine magazine in magazinesList)
            {
                getAllMagazinesView.Add(new GetAllMagazinesViewItem()
                {
                    Id = magazine.Id,
                    MagazineName = magazine.MagazineName,
                    MagazinePrice = magazine.MagazinePrice

                }); ;
            }
            return getAllMagazinesView;
        }
    }
}
