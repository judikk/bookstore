﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.RolesService;
using BookStore.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class RolesServise : IRoleService
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<User> _userManager;

        public RolesServise(RoleManager<IdentityRole> roleManager, UserManager<User> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async Task<bool> Create(CreateRoleView model)
        {
            if (!string.IsNullOrEmpty(model.RoleName))
            {
                var result = await _roleManager.CreateAsync(new IdentityRole(model.RoleName));
                if (result.Succeeded)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public async Task<bool> Delete(DeleteRoleView model)
        {
            var role = await _roleManager.FindByIdAsync(model.Id);
            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
                return true;
            }
            else
                return false;
        }

        public async Task<bool> Edit(EditRoleView model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user != null)
            {
                var rolesList = await _userManager.GetRolesAsync(user);
                if (rolesList.FirstOrDefault(x => x == model.RoleName) != null)
                {
                    await _userManager.RemoveFromRolesAsync(user, rolesList);
                    await _userManager.AddToRoleAsync(user, model.RoleName);
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> EditAll(EditAllRolesView model)
        {
            // получаем пользователя
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                // получаем список ролей, которые были добавлены
                var addedRoles = model.UserRoles.Except(userRoles);
                // получаем роли, которые были удалены
                var removedRoles = userRoles.Except(model.UserRoles);

                await _userManager.AddToRolesAsync(user, addedRoles);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                return true;
            }

            return false;
        }
    }
}

