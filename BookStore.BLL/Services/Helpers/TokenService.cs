﻿namespace BookStore.BLL.Services.Helpers
{
    public class TokenService
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
