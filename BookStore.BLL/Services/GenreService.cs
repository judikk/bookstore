﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.GenreView;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;
using System;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class GenreService : IGenreService
    {
        private IGenreRepository _genreRepository;
        private IBooksGenresRepository _booksGenresRepository;
        public GenreService(IGenreRepository genreRepository, IBooksGenresRepository booksGenresRepository)
        {
            _booksGenresRepository = booksGenresRepository;
            _genreRepository = genreRepository;
        }
        public async Task Add(AddGenreView model)
        {
            var genre = new Genre()
            {
                BookGenre = model.BookGenre,
                CreationDate = DateTime.UtcNow
            };
            await _genreRepository.Add(genre);
        }

        public async Task Delete(DeleteGenreView model)
        {
            var genre = await _genreRepository.Get(model.Id);
            var booksGenre = await _booksGenresRepository.GetAll();
            
            foreach (BooksGenres genres in booksGenre)
            {
                await _booksGenresRepository.Delete(genres);
            }
            await _genreRepository.Delete(genre);
        }

        public async Task<GetGenreView> Get(string id)
        {
            if (id == null)
            {
                throw new Exception("id is null");
            }
            var genre = await _genreRepository.Get(id);
            var authorGetView = new GetGenreView()
            {
                Id = genre.Id,
                BookGenre = genre.BookGenre
            };
            return authorGetView;
        }

        public async Task<GetAllGenresView> GetAll()
        {
            var genreList = await _genreRepository.GetAll();
            var genreGetViews = new GetAllGenresView();
            foreach (Genre genre in genreList)
            {
                genreGetViews.Genres.Add(new GenreGetAllGenresViewItem()
                {
                    Id = genre.Id,
                    BookGenre = genre.BookGenre
                }); ;
            }
            return genreGetViews;
        }

        public async Task Update(UpdateGenreView model)
        {
            var genre = await _genreRepository.Get(model.Id);
            genre.BookGenre = model.BookGenre;
            var booksGenre = await _booksGenresRepository.GetAll();
            
            foreach (BooksGenres genres in booksGenre)
            {
                await _booksGenresRepository.Update(genres);
            }
            await _genreRepository.Update(genre);
        }
    }
}
