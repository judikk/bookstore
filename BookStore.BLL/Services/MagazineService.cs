﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.MagazineService;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;
using System;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class MagazineService : IMagazineService
    {
        private IMagazineRepository _magazineRepository;
        private IBooksMagazinesRepository _booksMagazinesRepository;
        public MagazineService(IMagazineRepository magazineRepository, IBooksMagazinesRepository booksMagazinesRepository)
        {
            _booksMagazinesRepository = booksMagazinesRepository;
            _magazineRepository = magazineRepository;
        }
        public async Task Add(AddMagazineView model)
        {
            Magazine magazine = new Magazine()
            {
                MagazineName = model.MagazineName,
                CreationDate = DateTime.UtcNow,
                MagazinePrice = model.MagazinePrice
            };
            await _magazineRepository.Add(magazine);
        }

        public async Task Delete(DeleteMagazineView model)
        {

            var magazine = await _magazineRepository.Get(model.Id);
            var booksMagazines = await _booksMagazinesRepository.GetAll();
            
            foreach (BooksMagazines magazines in booksMagazines)
            {
                await _booksMagazinesRepository.Delete(magazines);
            }
            await _magazineRepository.Delete(magazine);

        }

        public async Task<GetMagazineView> Get(string id)
        {
            if (id == null)
            {
                throw new Exception("id is null");
            }
            Magazine magazine = await _magazineRepository.Get(id);
            GetMagazineView authorGetView = new GetMagazineView()
            {
                MagazineName = magazine.MagazineName,
                Id = magazine.Id,
                MagazinePrice = magazine.MagazinePrice
            };
            return authorGetView;
        }

        public async Task<GetAllMagazinesView> GetAll()
        {
            var magazineList = await _magazineRepository.GetAll();
            var magazineGetViews = new GetAllMagazinesView();
            foreach (Magazine magazine in magazineList)
            {
                magazineGetViews.Magazines.Add(new MagazineGetAllMagazinesViewItem()
                {
                    Id = magazine.Id,
                    MagazineName = magazine.MagazineName,
                    MagazinePrice = magazine.MagazinePrice
                }); ;
            }
            return magazineGetViews;
        }

        public async Task Update(UpdateMagazineView model)
        {
            var magazine = await _magazineRepository.Get(model.Id);
            magazine.MagazineName = model.MagazineName;
            magazine.MagazinePrice = model.MagazinePrice;
            var booksMagazine = await _booksMagazinesRepository.GetAll();
            
            foreach (BooksMagazines magazines in booksMagazine)
            {
                await _booksMagazinesRepository.Update(magazines);
            }
            await _magazineRepository.Update(magazine);
        }
    }
}
