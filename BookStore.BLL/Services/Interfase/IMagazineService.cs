﻿using BookStore.BLL.Views.MagazineService;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IMagazineService
    {
        Task Add(AddMagazineView model);
        Task Update(UpdateMagazineView model);
        Task Delete(DeleteMagazineView model);
        Task<GetMagazineView> Get(string id);
        Task<GetAllMagazinesView> GetAll();
    }
}
