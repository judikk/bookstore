﻿using BookStore.BLL.Views.RolesService;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IRoleService
    {
        Task<bool> Create(CreateRoleView model);
        Task<bool> Delete(DeleteRoleView model);
        Task<bool> Edit(EditRoleView model);
    }
}
