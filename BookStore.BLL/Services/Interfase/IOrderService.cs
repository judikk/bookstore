﻿using BookStore.BLL.Views.OrderService;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IOrderService
    {
        Task Add(AddOrderView model);
        Task Update(UpdateOrderView model);
        Task Delete(DeleteOrderView model);
        Task<GetOrderView> Get(string id);
        Task<GetAllOrdersView> GetAll();
    }
}
