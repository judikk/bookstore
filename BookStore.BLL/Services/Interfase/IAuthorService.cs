﻿using BookStore.BLL.Views.AuthorService;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IAuthorService
    {
        Task Add(AddAuthorView model);
        Task Update(UpdateAuthorView model);
        Task Delete(DeleteAuthorView model);
        Task<GetAuthorView> Get(string id);
        Task<GetAllAuthorsView> GetAll();
    }
}
