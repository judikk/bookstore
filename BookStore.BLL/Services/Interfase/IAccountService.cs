﻿using BookStore.BLL.Services.Helpers;
using BookStore.BLL.Views.UserService;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IAccountService
    {
        Task<TokenService> LogInEnabled(LoginUserView login);
        Task<string> RegisterEnabled(RegisterUserView model);
        Task<bool> ConfirmEmail(string token, string userName);
        Task<CreateRefreshTokenView> CreateRefreshToken(CreateRefreshTokenView tokenService);
        Task SendEmailAsync(string email, string subject, string message, string userName);
        }
}
