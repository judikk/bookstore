﻿using BookStore.BLL.Views.GenreView;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IGenreService
    {
        Task Add(AddGenreView model);
        Task Update(UpdateGenreView model);
        Task Delete(DeleteGenreView model);
        Task<GetGenreView> Get(string Id);
        Task<GetAllGenresView> GetAll();
    }
}
