﻿using BookStore.BLL.Views.BookService;
using BookStore.BLL.Views.BookService.BooksAuthorsService;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.BLL.Services.Interfase
{
    public interface IBookService
    {
        Task Add(AddBookView model);
        Task Update(UpdateBookView model);
        Task Delete(DeleteBookView model);
        Task<GetBookView> Get(string id);
        Task<GetAllBooksView> GetAll();
        Task<FilterBookView> Filter(FilterBookView item);
    }
}
