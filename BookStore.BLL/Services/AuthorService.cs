﻿using BookStore.BLL.Services.Interfase;
using BookStore.BLL.Views.AuthorService;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.InterfaseRepository;
using System;
using System.Threading.Tasks;

namespace BookStore.BLL.Services
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _authorRepository;
        private IBooksAuthorsRepository _booksAuthorsRepository;
        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public async Task Add(AddAuthorView model)
        {
            var author = new Author()
            {
                AuthorName = model.AuthorName,
                CreationDate = DateTime.UtcNow
            };
            await _authorRepository.Add(author);
        }

        public async Task Delete(DeleteAuthorView model)
        {
            var author = await _authorRepository.Get(model.Id);
            var booksAuthors = await _booksAuthorsRepository.GetAll();
            
            foreach (BooksAuthors authors in booksAuthors)
            {
                await _booksAuthorsRepository.Delete(authors);
            }
            await _authorRepository.Delete(author);
        }

        public async Task<GetAuthorView> Get(string id)
        {
            if (id == null)
            {
                throw new Exception("id is null");
            }
            var author = await _authorRepository.Get(id);
            var authorGetView = new GetAuthorView()
            {
                AuthorName = author.AuthorName,
                Id = author.Id
            };
            return authorGetView;
        }

        public async Task<GetAllAuthorsView> GetAll()
        {
            var authorList = await _authorRepository.GetAll();
            var authorGetViews = new GetAllAuthorsView();
            foreach (Author author in authorList)
            {
                authorGetViews.Authors.Add(new AuthorGetAllAuthorsViewItem()
                {
                    Id = author.Id,
                    AuthorName = author.AuthorName
                }); ;
            }
            return authorGetViews;
        }

        public async Task Update(UpdateAuthorView model)
        {
            var author = await _authorRepository.Get(model.Id);
            author.AuthorName = model.AuthorName;
            var booksAuthors = await _booksAuthorsRepository.GetAll();
            
            foreach (BooksAuthors authors in booksAuthors)
            {
                await _booksAuthorsRepository.Update(authors);
            }
            await _authorRepository.Update(author);
        }


    }
}
