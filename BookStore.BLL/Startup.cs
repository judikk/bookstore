﻿using BookStore.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.BLL
{
    public class Startup
    {
        public static IConfiguration Configuration { get; set; }

        public Startup()
        {
            Configuration = DAL.Startup.Configuration;
        }
        public static void ConfigureServices(IServiceCollection services, string connectionString)
        {
            DAL.Startup.ConfigureServices(services, connectionString);
            services.AddScoped<IOptionsDAL, OptionsDAL>(c => new OptionsDAL(connectionString));
            services.AddHttpContextAccessor();
        }
        public static void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            DAL.Startup.Configure(app, env);
        }
    }
}
